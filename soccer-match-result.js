import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-styles/paper-styles.js';
import '@polymer/iron-image/iron-image.js';

/**
 * `soccer-match-result`
 * Match result between two teams
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class SoccerMatchResult extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        
        .horizontal {
          @apply --layout-horizontal;
        }
        
        .vertical {
          @apply --layout-vertical;
        }
        
        .center {
          @apply --layout-center-justified;
        }
        .justified {
          @apply --layout-justified;
        }
        
        .text-center {
          text-align: center;
        }
        
        .number {
          @apply --paper-font-display4;
        }
        
        .place {
          @apply --paper-font-body2;
          color: var(--paper-gray-700);
        }
        
        .team {
          @apply --paper-font-subhead;
          color: var(--paper-gray-900);
        }
        
        iron-image {
          width: 100px;
          height: 100px;
        }
        
        .vr {
          border-left: 1px solid #E0E0E0;
        }
        
      </style>
      
      <div class="horizontal justified">
        <!-- Team one -->
        <div class="vertical center">
          <div>
            <iron-image src="[[team_one_image]]" sizing="cover"></iron-image>
          </div>
          <span class="team text-center">[[team_one]]</span>
        </div>
        
        <!-- Result one -->
        <div class="center">
          <span class="number">[[team_one_result]]</span>
        </div>
        
        <!-- Slash -->
        <div class="center">
          <span class="number">-</span>
        </div>
        
        <!-- Result two -->
        <div class="center">
          <span class="number">[[team_two_result]]</span>
        </div>
        
        <!-- Team two -->
        <div class="vertical center">
          <div>
            <iron-image src="[[team_two_image]]" sizing="cover"></iron-image>
          </div>
          <span class="team text-center">[[team_two]]</span>
        </div>
        
        <!-- Vr -->
        <div class="vr"></div>
        
        <!-- Place -->
        <div class="vertical center">
          <span class="place text-center">[[place]]</span>
          <span class="place text-center">[[time]]</span>
        </div>
      </div>
    `;
  }
  
  static get properties() {
    return {
      team_one_image: {
        type: String,
        value: 'https://i.imgur.com/sDsLwAP.png'
      },
      team_one: {
        type: String,
        value: 'Equipo 1'
      },
      team_two_image: {
        type: String,
        value: 'https://i.imgur.com/sDsLwAP.png'
      },
      team_two: {
        type: String,
        value: 'Equipo 2'
      },
      team_one_result: {
        type: Number,
        value: 0
      },
      team_two_result: {
        type: Number,
        value: 0
      },
      place: {
        type: String,
        value: 'City Town'
      },
      time: {
        type: String,
        value: '00:00 hrs'
      }
    };
  }
  
}

window.customElements.define('soccer-match-result', SoccerMatchResult);
